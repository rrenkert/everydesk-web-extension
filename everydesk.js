var tab;
var activeTab;
function sendMsg() {
    console.log(activeTab);
    chrome.tabs.sendMessage(tab.id, {
        title: activeTab.title,
        url: activeTab.url,
        favIcon: activeTab.favIconUrl
    });
    chrome.notifications.create({
        "type": "basic",
        "iconUrl": chrome.extension.getURL("icons/favicon.png"),
        "title": "Everydesk",
        "message": "Send \"" + activeTab.title + "\" as Favorite to everydesk!"
   });
}
function errorLog(err) {
    console.log(err);
}

function execScript(tabs) {
    tab = tabs[0];
    var exec = chrome.tabs.executeScript(tabs[0].id, {
        file: '/content_script.js'
    }, sendMsg);
}

function handleClick() {
    chrome.tabs.query({active: true}, function(tabs) {
        activeTab = tabs[0];
    });
    chrome.tabs.query({title: "everydesk"}, execScript);
}
chrome.browserAction.onClicked.addListener(handleClick);

